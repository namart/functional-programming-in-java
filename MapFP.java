import java.util.Vector;

public class MapFP { 

  public static interface IteratorFunction<E> {
    public E visit(E item);
  }

  public static <E> Vector<E> map(IteratorFunction<E> func , Vector<E> input) {

    Vector<E> output = mapHelper(func, input, new Vector<E>(), 0);
    return output;

  }

  private static <E> Vector<E> mapHelper(IteratorFunction<E> func, Vector<E> input, Vector<E> output, int i) {

    output.add(func.visit(input.get(i)));
    if(output.size() == input.size()) {
      return output;
    } else {
      return mapHelper(func, input, output, ++i);
    }

  }

  public static void main(String ... args) { 

    Vector<String> v = new Vector<String>();
    v.add("hallo");
    v.add("du");
    v.add("mensch");

    Vector<String> result = map((item) -> {
      return item + " ..."; 
    }, v);
    System.out.println(result);
  }
}
