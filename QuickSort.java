import java.util.Vector;

public class QuickSort { 

  public static interface TrivialFunction {
    public Boolean visit(Vector<Integer> list);
  }

  public static interface SolveFunction {
    public Vector<Integer> visit(Vector<Integer> list);
  }

  public static interface DivideFunction {
    public Vector<Vector<Integer>> visit(Vector<Integer> list);
  }

  public static interface CombineFunction {
    public Vector<Integer> visit(Vector<Integer> list1, Vector<Integer> list2);
  }

  public static Vector<Integer> divideAndConquer(TrivialFunction trivial, SolveFunction solve, DivideFunction divide, CombineFunction combine, Vector<Integer> list) {

    if(trivial.visit(list)) {
      return solve.visit(list);
    } else {
      Vector<Vector<Integer>> lists = divide.visit(list);
      Vector<Integer> list1 = divideAndConquer(trivial, solve, divide, combine, lists.get(0));
      Vector<Integer> list2 = divideAndConquer(trivial, solve, divide, combine, lists.get(1));
      return combine.visit(list1, list2);
    }
  
  }

  public static Vector<Integer> swap(Vector<Integer> list, Integer i, Integer j) {
    Integer temp = list.get(i);
    list.set(i, j);
    list.set(j, temp);
    return list;
  }

  public static Integer divideFindLeftHelper(Vector<Integer> list, Integer i, Integer pivot) {
    if(list.get(i) <= pivot) {
      divideFindLeftHelper(list, ++i, pivot);
    } else {
      return i;
    }
  }

  public static Integer divideFindRightHelper(Vector<Integer> list, Integer j, Integer pivot) {
    if(list.get(j) <= pivot) {
      divideFindRightHelper(list, ++j, pivot);
    } else {
      return j;
    }
  }

  public static Integer divideHelper(Vector<Integer> list, Integer pivot) {

      Integer i = 0;
      Integer j = list.size()-1;

      i = divideFindLeftHelper(list, i, pivot);
      j = divideFindRightHelper(list, j, pivot);

      if(i < j) {
        swap(list, i, j);
      } else {
        return i; 
      }

      divideHelper(list, pivot);

  }

  public static void main(String ... args) { 

    Vector<Integer> v = new Vector<Integer>();
    v.add(1);
    v.add(5);
    v.add(8);
    v.add(7);
    v.add(9);
    v.add(4);
    v.add(2);

    TrivialFunction trivial = (list) -> {
      if(list.size() <= 1) {
        return true;
      } else {
        return false; 
      }
    };

    SolveFunction solve = (list) -> {
      return list;
    };

    DivideFunction divide = (list) -> {

      Integer pivot = list.lastElement();

      Integer i = divideHelper(list, pivot);

      if(list.get(i) > pivot) {
        list = swap(list, i, list.size()-1);
      }

      // aufteilen in zwei listen
      Vector<Vector<Integer>> lists = new Vector<Vector<Integer>>();
      List<Integer> list2 = list.subList(i, list.size()-1); 
      lists.add(new Vector(list));
      lists.add(list2);
      return lists;

    };

    CombineFunction combine= (list1, list2) -> {
      Vector<Integer> newList = new Vector<Integer>(list1);
      return newList.addAll(list2);
    };

    Vector<Integer> result = divideAndConquer(trivial, solve, divide, combine, v);

    System.out.println(result);
  }
}
