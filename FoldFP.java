import java.util.Vector;

public class FoldFP { 

  public static interface IteratorFunction {
    public Integer visit(Integer prev, Integer curr);
  }

  public static Integer fold(IteratorFunction func , Integer memo, Vector<Integer> list) {
    return foldHelper(func, memo, list, 0);
  }

  private static Integer foldHelper(IteratorFunction func , Integer memo, Vector<Integer> list, int i) {

    Integer newValue = func.visit(memo, list.get(i));

    if(list.size() == i+1) {
      return newValue;
    } else {
      return foldHelper(func, newValue, list, ++i);
    }

  }

  public static void main(String ... args) { 

    Vector<Integer> v = new Vector<Integer>();
    v.add(1);
    v.add(2);
    v.add(3);

    Integer result = fold((prev, curr) -> {
      return prev + curr;
    }, 10, v);

    System.out.println(result);
  }
}
