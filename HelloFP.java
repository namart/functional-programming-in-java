
public class HelloFP {

    interface HelloFunction {
        void hello(Integer i);
    }

    public static void main(String... args) {
		HelloFunction f = (i) -> {
            System.out.println("hello functional world " + i);
        };

        f.hello(4);
    }
}
